//function for ajax pagination
function edd_points_ajax_pagination(pid){
	var data = {
					action: 'edd_points_next_page',
					paging: pid
				};

			jQuery('.edd-points-sales-loader').show();
			jQuery('.edd-points-paging').hide();

			jQuery.post(EDDPoints.ajaxurl, data, function(response) {
				var newresponse = jQuery( response ).filter( '.edd-points-user-log' ).html();
				jQuery('.edd-points-sales-loader').hide();
				jQuery('.edd-points-user-log').html( newresponse );
			});
	return false;
}

jQuery(document).ready(function($){
	$('body').on('edd_quantity_updated', function(e, data){
		if ( 'object' === typeof data ) {
			$.each(data, function(index, value){
				if(index.indexOf("points_redeem") >= 0){
					id = '#edd_cart_fee_'+index+' .edd_cart_fee_amount';
					$(id).html(value);
				}
			});
		}
	});
	$('body').on('click', '#edd_points_apply_discount', function (){
		var postData = {
				action: 'edd_points_process_discount',
				action_mode: 'apply_discount',
				ajax_mode: 'true'
			};

			$.ajax({
				type: "POST",
				data: postData,
				dataType: "json",
				url: edd_global_vars.ajaxurl,
				xhrFields: {
					withCredentials: true
				},
				success: function (response) {
					if(response){
						$('.edd_cart_amount').text(response.total);
						$('#edd_cart_discounts').show();
						$(response.discount_row).insertAfter('#edd_cart_discounts');
						$('fieldset.edd-points-redeem-points-wrap').replaceWith(response.box_html);
						$('html, body').animate({
            	scrollTop: $('#edd_checkout_cart_wrap').offset().top
        		}, 0);
					}
				}
			}).fail(function (data) {
				if ( window.console && window.console.log ) {
					console.log( data );
				}
			});

			return false;
	});
	$('body').on('click', '#edd_points_remove_discount', function (){
		var postData = {
				action: 'edd_points_process_discount',
				action_mode: 'remove_discount',
				ajax_mode: 'true'
			};

			$.ajax({
				type: "POST",
				data: postData,
				dataType: "json",
				url: edd_global_vars.ajaxurl,
				xhrFields: {
					withCredentials: true
				},
				success: function (response) {
					if(response){
						$('#edd_cart_discount_points').remove();
						if(response.hide_header){
							$('#edd_cart_discounts').hide();
						}
						$('.edd_cart_amount').text(response.total);
						$('fieldset.edd-points-redeem-points-wrap').replaceWith(response.box_html);
						$('html, body').animate({
            	scrollTop: $('#edd_checkout_cart_wrap').offset().top
        		}, 0);
					}
			}
			}).fail(function (data) {
				if ( window.console && window.console.log ) {
					console.log( data );
				}
			});

			return false;
	});
});
